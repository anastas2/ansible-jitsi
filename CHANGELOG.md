# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.3](https://gitlab.com/guardianproject-ops/ansible-jitsi/compare/0.1.2...0.1.3) (2020-07-24)


### Bug Fixes

* Automatically restart JVB process after a crash ([42efcfa](https://gitlab.com/guardianproject-ops/ansible-jitsi/commit/42efcfabe72ab034263a512ae004b13040585d2a))

### [0.1.2](https://gitlab.com/guardianproject-ops/ansible-jitsi/compare/0.1.1...0.1.2) (2020-07-18)


### Features

* Add config for registering secure domain prosodyctl users ([883c797](https://gitlab.com/guardianproject-ops/ansible-jitsi/commit/883c797a7ead14d8cd11b367d00cbdeb4aa15789))

### [0.1.1](https://gitlab.com/guardianproject-ops/ansible-jitsi/compare/0.1.0...0.1.1) (2020-07-17)


### Features

* Allow configuration of videobridge's JVM heapsize ([80a1289](https://gitlab.com/guardianproject-ops/ansible-jitsi/commit/80a12897488a22e81aa26714862d45835f458bb6))
- Allow configuration of ssl cert+key path
- [jitsiexporter](https://github.com/xsteadfastx/jitsiexporter) for JVB monitoring
- Fixed secure domain config
- Pinned prosody version to 0.11.5-1
- Use jitsi stable branch by default

## [0.1.0] - 2020-04-13

### Added


- This CHANGELOG
- First version

### Changed

n/a

### Removed

n/a

[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-jitsi/-/tags/0.1.0
