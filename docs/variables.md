## Role Variables

* `jitsi_version`: `stable` - select which version of Jitsi to install. `stable` or `nightly`. The latest version will be installed.



* `jitsi_meet_prosody_version`: `0.11.*` - The deb package version of prosody to install



* `jitsi_last_n`: `5` - How many video streams are transmitted by default.


  set to `-1` to enable default behaviour (all video streams of all participants are shared with everybody. Puts a significant amount of load on the videobridges.
  ```yaml
  jitsi_last_n: -1
  ```

* `jitsi_notice_message`: `''` - Message to show the users. Example: 'The service will be down for maintenance at 01:00 AM GMT, empty string to disable



* `jitsi_meet_videobridge_secret`: `'jvb_secret'` - 



* `jitsi_meet_videobridge_password`: `'jvb_password'` - 



* `jitsi_meet_jicofo_secret`: `'jicofo_secret'` - 



* `jitsi_meet_jicofo_password`: `'jicofo_pw'` - 



* `jitsi_meet_videobridge_ram_max_mb`: `3072` - The maximum size of the JVB's heap size, in megabytes.



* `jitsi_meet_secure_domain_enabled`: `false` - set to true to enable secure domain, you'll need to manage your own users



* `jitsi_meet_secure_domain_users`: `[]` - a list of dicts with the keys 'username' and 'password'.



* `jitsi_ssl_cert_path`: `/etc/ssl/{{ jitsi_fqdn }}.crt` - the remote path to where your cert file is stored. You must place this file there yourself.



* `jitsi_ssl_key_path`: `/etc/ssl/{{ jitsi_fqdn }}.key` - the remote path to where your key file is stored. You must place this file there yourself.



* `jibri_enable`: `false` - whether to enable jibri or not



* `jibri_recording`: `false` - whether to enable recording with jibri



* `jibri_streaming`: `false` - whether to enable streaming with jibri



* `jibri_user_jibri_password`: `"jibriauthpass"` - 



* `jibri_user_recorder_password`: `"jibrirecorderpass"` - 



* `jitsi_brand_footer_items`: `see defaults/main.yml` - A list of label/url pairs to display at the footer of the welcome page


  ```yaml
  jitsi_brand_footer_items:
    - label: Help
      url: https://guardianproject.info
    - label: Legal Notice
      url: https://guardianproject.info
  ```

* `jitsi_brand_name`: `Keanu Meet` - the name of your jitsi meet instance



* `jitsi_brand_org`: `The Guardian Project` - The name of your organization



* `jitsi_brand_left_watermark_url`: `https://gitlab.com/guardianproject/guardianprojectpublic/-/raw/master/Graphics/GuardianProject/pngs/logo-black-w256.png` - a url to a transparent png that will be layered over every call and on the welcome page



* `jitsi_brand_url`: `https://guardianproject.info` - A link to your organization's site



* `jitsi_brand_support_url`: `https://guardianproject.info` - A link to your organizationan's support or contact page



* `jitsi_octo_enabled`: `false` - Whether to enable octo or not



* `jitsi_private_ip`: `undefined` - The private local ip octo and prometheus exporter should bind to



* `jitsi_region`: `undefined` - The region this instance is in



* `jitsi_use_maxmind_free`: `true` - set to false if you have a paid subscription to maxmind



* `jitsi_octo_nginx_geoip_mapping_vars`: `"$geoip2_data_country_code:$geoip2_data_continent_name"` - 



* `jitsi_octo_nginx_regions_mapping`: `see defaults/main.yml` - the region mapping


  ```yaml
  jitsi_octo_nginx_regions_mapping:
    - default us-east-2
    - ~^.*:EU eu-central-1
    - ~^.*:AF eu-central-1
    - ~^.*:NA us-east-2
    - ~^.*:SA us-east-2
    - ~^.*:AN us-east-2
    - ~^.*:AS ap-south-1
    - ~^.*:OC ap-south-1
  ```

* `jitsi_nginx_conf_path`: `/etc/nginx/conf.d` - the path to where the nginx geoip config should be placed



* `jitsi_nginx_vhost_path`: `/etc/nginx/conf.d` - the path to where the jitsi nginx vhost config should be placed


