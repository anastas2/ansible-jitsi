import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


def test_hosts_file(host):
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

@pytest.mark.parametrize(
    "name,content",
    [
        ("/etc/jitsi/videobridge/sip-communicator.properties", ""),
        ("/etc/jitsi/videobridge/config", "JVB_HOSTNAME=main.jitsi.example.com"),
        ("/etc/jitsi/videobridge/config", "VIDEOBRIDGE_MAX_MEMORY=3072m"),
        ("/etc/systemd/system/jitsi-exporter.service", "/usr/local/bin/jitsi-exporter -url http://localhost:8080/colibri/stats"),
    ],
)
def test_files(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)


@pytest.mark.parametrize("name", ["jitsi-videobridge2"])
def test_services(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled
