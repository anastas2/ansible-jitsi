import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


@pytest.mark.parametrize(
    "name,content",
    [
        ("/etc/prosody/conf.d/jitsi.example.com.cfg.lua", "jitsi.example.com"),
        ("/etc/prosody/certs/jitsi.example.com.key", ""),
        ("/etc/prosody/certs/jitsi.example.com.crt", ""),
        ("/etc/apt/preferences.d/prosody", "Pin: version"),
        (
            "/etc/nginx/conf.d/90-jitsi.example.com.conf",
            "ssl_certificate /etc/ssl/jitsi.example.com.crt",
        ),
    ],
)
def test_files(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)


@pytest.mark.parametrize("name", ["nginx", "jicofo", "prosody"])
def test_services(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled
